<?php
/**
 * @file
 * basic_configuration.features.inc
 */

/**
 * Implements hook_node_info().
 */
function basic_configuration_node_info() {
  $items = array(
    'sample' => array(
      'name' => t('Sample'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
