api = 2
core = 7.x

projects[drupal][version] = 7.39

projects[admin_menu][version] = 3.0-rc5
projects[admin_menu][subdir] = contrib

projects[ctools][version] = 1.9
projects[ctools][subdir] = contrib

projects[diff][version] = 3.2
projects[diff][subdir] = contrib

projects[features][version] = 2.6
projects[features][subdir] = contrib

projects[variable][version] = 2.5
projects[variable][subdir] = contrib

projects[views][version] = 3.11
projects[views][subdir] = contrib

projects[adminimal_theme][version] = 1.20
